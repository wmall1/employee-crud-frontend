import react from 'react';
import './login.css';
import Dashboard from './Dashboard';
import SignUp from './SignUp';
import {Link} from 'react-router-dom'


export default class Login extends react.Component
{
    constructor()
    {
        super();
        this.state = {
            isSignUp :false,
            username:"",
            password:"",
            success : false,
            currUname : "",
            token : ""
        }
        this.onchange = this.onchange.bind(this);
        this.login = this.login.bind(this);
        this.logout = this.logout.bind(this);
        this.signupHandler = this.signupHandler.bind(this);
        this.LoginPage = this.LoginPage.bind(this);
    }

    onchange(e)
    {
        this.setState({
            [e.target.name]:e.target.value
        })
    }

    async login(e)
    {
        
        const uname = this.state.currUname;
        const pass = this.state.password;

        if(uname.trim()!=="" && pass.trim()!=="")
        {
          const response  = await fetch('http://localhost:8888/login',{
                method:"POST",
                
                body: JSON.stringify({username: uname, password: pass}),
                headers:{
                    "Content-type":"application/json",
                },
            })

            const data = await response.json();
            //console.log(data.accessToken);
        
                if(data.accessToken.trim()!=="")
                {
                    this.setState({
                        success:true,
                        username:uname,
                        currUname:"",
                        password:"",
                        token : data.accessToken
                    })
                    
                    await localStorage.setItem('login',JSON.stringify({
                        isLogin : true,
                        token : data.accessToken
                    }))
                    console.log(this.state.token);
                    window.location = '/employee';
                }
                else
                {
                    alert("userName or Password is incorrect");
                }
              
        }
        else
        {
            alert("ussename and password must not be empty");
        }
    }

    logout(e)
    {
        this.setState({
            success:false
        })
    }

    signupHandler()
    {
        this.setState({
            isSignUp:true
        })
    }

    render(){
        return (

            this.state.isSignUp ? <SignUp /> : (
            
            this.state.success ? <Dashboard username = {this.state.username}
                logout = {this.logout}
                token ={this.state.token}
                /> : (
                    this.LoginPage()
            ))
        )
    }


    LoginPage = ()=> {
        return (
            <div class="wrapper fadeInDown">
                    <div id="formContent">
                        <div class="fadeIn first">
                        </div>
                        
                        <input type="text" class="fadeIn second" name="currUname" placeholder="User Name"
                            value = {this.state.currUname} 
                            onChange={this.onchange}
                        />
                        <input type="password" class="fadeIn third" name="password" placeholder="password" 
                            value = {this.state.password}
                            onChange={this.onchange}
                        />
                        <button class="fadeIn fourth loginBtn" value="Login" onClick={this.login}>Login</button>
                        
    
    
                        <div id="formFooter">
                             <Link to="/signup" class="underlineHover"  onClick={this.signupHandler}>SignUp</Link>
                        </div>
    
                    </div>
                </div>
        )
    }



}