import React from 'react';
import { Link } from 'react-router-dom';
import Add from './Add';
import Edit from './Edit';
import './dashboard.css';


export default class Dashboard extends React.Component
{
    constructor()
    {
        super();
        this.state = {
            tableData : [],
            isEdit : false,
            idx : ""
            
       }
       
        this.createEmp = this.createEmp.bind(this);
        this.deleteRecord = this.deleteRecord.bind(this);
        this.editRecord = this.editRecord.bind(this);
    }

    async componentDidMount()
    {
        const status = await JSON.parse(localStorage.getItem('login'));

        if(status.isLogin)
        {
            const response = await fetch("http://localhost:8888/employee",{
                headers:{
                    authorization : status.token
                }
            });
            
            const data = await response.json();
            console.log(data);
            this.setState({
                tableData : data
            })
        }
        
    }

    createEmp()
    {
        
        window.location = '/add';
    }

    editRecord(e,idx)
    {
        
        this.setState({
            idx : idx,
            isEdit :true
        });
        
        window.location=`/edit/${idx}`;
    }

    async deleteRecord(e,idx)
    {
        //e.confirm("Are you really want to delte the record");
        console.log(idx);
        if(idx.trim()!=="")
        {
            const response = await fetch(`http://localhost:8888/employee/${String(idx)}`,{
                method:"DELETE"
            });

            const data = await response.json();
            console.log(data);
            window.location= '/employee';
        }
    }

    

    render(){
        
        const rowData = this.state.tableData.map((item)=>{
            return(
                <tr key={item._id}>
                    <td>{item.fname}</td>
                    <td>{item.lname}</td>
                    <td>{item.email}</td>
                    <td>{item.Designation}</td>
                    <td>
                        <span ><i onClick={(e)=>{this.editRecord(e,item._id)}} class="fa fa-pencil fa-lg" aria-hidden="true"></i></span>
                        <span onClick={(e)=>this.deleteRecord(e,item._id)}><i class="fa fa-trash fa-lg" aria-hidden="true"></i></span>
                    </td>
                </tr>
            )
        })
        
        return(
            
                this.state.isEdit ? (<Edit id = {this.state.idx}/>) :
                <>
                    <h3><a class="btn btn-secondary" onClick={this.createEmp}><i class="fa fa-plus"></i> Create New</a> Employee List</h3>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Email</th>
                                <th>Designation</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            {rowData}
                        </tbody>
                    </table>
                </> 
            
        )
    }
}