import react from 'react';
import './SignUp.css';
import Login from './Login';

class SignUp extends react.Component 
{
    constructor()
    {
        super();
        this.state = {
            isLogin : false,
            gender :"male",
            FirstName :"",
            LastName :"",
            Email :"",
            PassWord:"",
            ConfirmPassWord:"",
            Occupation:"",
            PhoneNumber:"",
            isAccCreated : false,
        }

        this.inputHandler = this.inputHandler.bind(this);
        this.handleOptionChange = this.handleOptionChange.bind(this);
        this.register = this.register.bind(this);
    }

    handleOptionChange(e) {
        this.setState({
          gender: e.target.value
        });

        // console.log(this.state.gender);
    }
    
    inputHandler(e){
        this.setState({
            [e.target.name] : e.target.value
        })
    }

    register()
    {
        const gen = this.state.gender;
        const fname = this.state.FirstName;
        const lname = this.state.LastName;
        const email = this.state.Email;
        const pass = this.state.PassWord;
        const cpass = this.state.ConfirmPassWord;
        const occup = this.state.Occupation;
        const phone = this.state.PhoneNumber;
        
        if(gen.trim()!=="" && fname.trim()!=="" && lname.trim()!=="" && email.trim()!=="" && pass.trim()!=="" 
        && cpass.trim()!=="" && occup.trim()!=="" && phone.trim()!=="")
        {
            fetch('http://localhost:8888/signup',{
                method:"POST",
                mode: "no-cors",
                body: JSON.stringify({
                    firstname : fname,
                    lastname : lname,
                    username : email,
                    password : pass,
                    phone : phone ,
                    occupation : occup,
                    gender : gen
                }),
                headers:{
                    "Content-type":"application/json",
                },
            }).then((r)=>{
                if(r.ok)
                    return ({succed:true})
                else
                    return r.json();
            }).then((r)=>{
                if(r.succed===true)
                {

                    this.setState({
                        gender :"male",
                        FirstName :"",
                        LastName :"",
                        Email :"",
                        PassWord:"",
                        ConfirmPassWord:"",
                        Occupation:"",
                        PhoneNumber:"",
                        isAccCreated:true
                    })
                }
                else
                {
                    alert("Something Went Wrong");
                }
            })

        }
        else
        {
            alert("All fields are necessary");
        }
    }
      
    render()
    {
        
        return (
            this.state.isLogin ? <Login /> : 
            <div className="container register">
                <div className="row">
                    <div className="col-md-3 register-left">
                        <img src="https://image.ibb.co/n7oTvU/logo_white.png" alt=""/>
                        <h3>Welcome</h3>
                        <p></p>
                        <input type="submit" name="" value="Login" onClick={()=>this.setState({
                            isLogin:true
                        })}/><br/>
                    </div>
                    <div className="col-md-9 register-right">
                        
                        <div className="tab-content" id="myTabContent">
                            <div className="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                            <h3 class="register-heading">Sign Up</h3>
                                <div className="row register-form">
                                    <div className="col-md-6">
                                        <div className="form-group">
                                            <input type="text" className="form-control" placeholder="First Name *" 
                                                value={this.state.FirstName} 
                                                name="FirstName"
                                                onChange={this.inputHandler}
                                            />
                                        </div>
                                        <div className="form-group">
                                            <input type="text" className="form-control" placeholder="Email *" 
                                                value={this.state.Email} 
                                                name="Email"
                                                onChange={this.inputHandler}
                                            />
                                        </div>
                                        <div className="form-group">
                                            <input type="password" className="form-control" placeholder="Password *" 
                                                value={this.state.PassWord} 
                                                name="PassWord"
                                                onChange={this.inputHandler}
                                            />
                                        </div>
                                        <div className="form-group">
                                            <input type="text" className="form-control"  placeholder="Occupation *" 
                                                value={this.state.Occupation} 
                                                name="Occupation"
                                                onChange={this.inputHandler}
                                            />
                                        </div>
                                        
                                    </div>
                                    

                                    <div className="col-md-6">
                                        <div className="form-group">
                                            <input type="text" className="form-control" placeholder="Last Name *" 
                                                value={this.state.LastName} 
                                                name="LastName"
                                                onChange={this.inputHandler}
                                            />
                                        </div>
                                        <div className="form-group">
                                            <input type="text" className="form-control" placeholder="Phone Number *" 
                                                value={this.state.PhoneNumber} 
                                                name="PhoneNumber"
                                                onChange = {this.inputHandler}
                                            />
                                        </div>
                                        <div className="form-group">
                                            <input type="password" className="form-control" placeholder="Confirm Password *" 
                                                value={this.state.ConfirmPassWord} 
                                                name="ConfirmPassWord"
                                                onChange={this.inputHandler}
                                            />
                                        </div>


                                        <div className="form-group">
                                            <div className="maxl">
                                                <label className="radio inline"> 
                                                    <input type="radio" name="gender" value="male" checked={this.state.gender==='male'}
                                                    onChange= {(e)=>this.handleOptionChange(e)}
                                                    />
                                                    <span> Male </span> 
                                                </label>
                                                <label className="radio inline"> 
                                                    <input type="radio" name="gender" value="female"
                                                    onChange={(e)=>this.handleOptionChange(e)}
                                                    />
                                                    <span>Female </span> 
                                                </label>
                                            </div>
                                        </div>

                                        
                                    </div>    
                                        <div className="btnRegDiv">
                                            <button className="btnRegister" onClick={this.register}>Register</button>
                                        </div>
                                        <div className="accCreationMsg">
                                            {this.state.isAccCreated ? <h3>Account Created Successfully</h3> : null }
                                        </div>  
                                </div>
                               
                            </div>
                           
                        </div>
                    </div>
                </div>

            </div>
        )
    }
}


export default SignUp;