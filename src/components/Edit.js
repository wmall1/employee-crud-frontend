import React from 'react';
import {Form,Button} from 'react-bootstrap'
import './addoredit.css';
import {Link} from 'react-router-dom'

export default class Edit extends React.Component
{
    constructor()
    {
        super();
        this.state={
            _id : "",
            fname : "",
            lname: "",
            email : "",
            desc : "",
            msg : "",
        }
        this.handleInput = this.handleInput.bind(this);
        this.save = this.save.bind(this);
    }

    handleInput(e)
    {
        this.setState({
            [e.target.name] : e.target.value
        })
    }

    async componentDidMount()
    {
        const status = await JSON.parse(localStorage.getItem('login'));
        if(status.isLogin)
        {
            console.log();
            const {id} = this.props.match.params;
            const response = await fetch(`http://localhost:8888/employee/${String(id)}`,{
                method : "GET",
                headers :{
                    authorization : status.token 
                }
            });

            const data = await response.json();
            console.log(data);
            this.setState({
                _id : data.employee._id,
                fname : data.employee.fname,
                lname : data.employee.lname,
                email : data.employee.email,
                desc : data.employee.Designation
            })
        }
    }

    async save()
    {
        const id = this.state._id;
        const fname = this.state.fname;
        const lname = this.state.lname;
        const email = this.state.email;
        const desc = this.state.desc;
        if(id.trim()!=="" && fname.trim()!=="" && lname.trim()!=="" && email.trim()!=="" && desc.trim()!=="")
        {
            const status = JSON.parse(localStorage.getItem('login'));
            if(status.isLogin)
            {
                const response = await fetch(`http://localhost:8888/employee/${id}`,{
                    method: "PUT",
                    headers : {
                        authorization : status.token,
                        "Content-type":"application/json",    
                    },
                    body : JSON.stringify({
                        fname : fname,
                        lname : lname,
                        email : email,
                        Designation : desc    
                    })
                });

                const data = await response.json();
                console.log(data);
                //window.location = 'employee';
                this.setState({
                    msg : "Record Updated Successfully"
                })
            }
            
        }
        else
        {
            alert("All fields are necessary");
        }
        
    }

    render(){
        return (
            <div className="mainform">
                <h1>Edit Employee</h1>
          <Form.Group className="textbox">
            <Form.Control  size="lg" type="text" placeholder="First Name" name="fname" 
                value={this.state.fname}
                onChange={this.handleInput}
            />
            <br />
            <Form.Control  size="lg" type="text" placeholder="Last Name" name="lname" 
                value={this.state.lname}
                onChange={this.handleInput}
            />
            <br />
            <Form.Control  size="lg" type="text" placeholder="Email" name="email" 
                value={this.state.email}
                onChange={this.handleInput}
            />
            <br />
            <Form.Control  size="lg" type="text" placeholder="Designation" name="desc" 
                value={this.state.desc}
                onChange={this.handleInput}
            />
            <br />
            <div className="btnGroup">
            <Button className="btnSubmit" onClick={this.save}>Update</Button>
            <Link to="/employee" className="btnSubmit">View All</Link>
            </div>
            <br />
            <h4>{this.state.msg}</h4>
          </Form.Group>
          </div>
        );
    }
}
