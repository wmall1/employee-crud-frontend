
import './App.css';
import Login from './components/Login'
import {BrowserRouter,Route,Switch} from 'react-router-dom';
import Dashboard from './components/Dashboard';
import Add from './components/Add';
import SignUp from './components/SignUp';
import Edit from './components/Edit';
function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Switch>
          <Route exact path="/">
            <Login />
          </Route>
          <Route exact path="/signup">
            <SignUp />
          </Route>
          <Route exact path="/employee">
            <Dashboard />
          </Route>
          <Route exact path="/add">
            <Add />
          </Route>
          <Route exact path="/edit/:id" component={Edit} />
          
        </Switch>
      </BrowserRouter>
      
    </div>
  );
}

export default App;
